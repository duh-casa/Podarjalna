/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Podarjalna.

    Podarjalna is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Podarjalna is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Podarjalna.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

CREATE TABLE `izdelki` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `slika` text,
  `opis` text,
  `ohranjenost` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `licitacija` (
  `id` int(11) NOT NULL,
  `znesek` int(11) DEFAULT '0',
  `kontakt` varchar(45) DEFAULT NULL,
  `zacetek` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
