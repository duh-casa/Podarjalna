<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Podarjalna.

    Podarjalna is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Podarjalna is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Podarjalna.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

spl_autoload_register(function ($class_name) { require_once "modules/".$class_name.'.php'; });

$c = new captcha();

$doc = new html("Duh Časa - Podarjalna", array(
 "bootstrap" => True,
 "css" => "slog.css",
	"js" => $c->js()
));

?><h1>Podarjalna</h1>

<p>Dobrodošli v <a href="http://www.duh-casa.si/" target="_blank">Duh Časa</a> Podarjalni.</p>

<p>Na tej spletni strani imamo objavljene zanimivejše tehnične predmete iz naše <a href="http://racunalniki.duh-casa.si/" target="_blank">delavnice</a>, ki jih ne moremo podariti socialno ogroženim, ker so za ta namen neprimerne. Naša želja je, da bi ti predmeti našli pot do ljudi, ki jih potrebovali za potrebe reševanja specifičnih potreb za katere so le-ti namenjeni.</p>

<p>Stran deluje po principu licitacije, pri čemer je "kupnina" dejansko vaša donacija društvu. Zadev ne zaračunavamo z namenom da bi zbirali dobiček, temveč zato ker upamo da bodo tako predmeti najverjetneje končali v rokah ljudi, ki jih imajo namen dejansko uporabiti, oziroma jih najbolj potrebujejo. Vsa oprema se podaja "izpraznjena" torej v njej ni več podatkov prejšnjih uporabnikov.</p>

<p>Stran je trenutno še v preizkusni različici. Licitacija še ni dejanska ali obvezujoča.</p>

<?php

$i = new items();

if(!isset($_GET["id"]) || $_GET["id"] == 0) {
?><h1>Predmeti</h1><br><?php
 $i->getItems();
} else {
 $doc->addJs("scrollDown", '
 $( document ).ready(function() {
  $("html, body").delay(800).animate({ scrollTop: $(document).height() }, "slow");
 });
 ');
?><br><a href=".">&lt;&lt; Nazaj</a><?php
 $i->getItem($_GET["id"]);
}

?>
