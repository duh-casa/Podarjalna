<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Podarjalna.

    Podarjalna is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Podarjalna is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Podarjalna.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "mysqli.php";

class licitacija {

	private $db;
	
	private $odprtaDni = 3;

	function __construct() {
		$this->db = new dblink();
	}
	
 function cena($id) {
  $izdelek = $this->db->q("
   SELECT `znesek` 
     FROM `licitacija` 
    WHERE `id` = '".$this->db->e($id)."'
    LIMIT 1
  ");
  
  if(isset($izdelek[0])) {
   return $izdelek[0]["znesek"];
  } else {
   return 0; 
  }
  
 }
 
 function ponudba($id, $znesek = 1, $kontakt = "") {
  
  if(trim($kontakt) != "" && floor($znesek) > $this->cena($id)) {
   $this->db->q("
    INSERT INTO `licitacija` (`id`, `znesek`, `kontakt`, `zacetek`)
    VALUES ('".$this->db->e($id)."', '".$this->db->e($znesek)."', '".$this->db->e($kontakt)."', NOW())
    ON DUPLICATE KEY UPDATE
     `znesek` = '".$this->db->e($znesek)."',
     `kontakt` = '".$this->db->e($kontakt)."'
   ");
   return true;
  } else {
   return false;
  }
 }
 
 function obrazec($id) {
  
  if($this->mozna($id)) { //Če ni možna ne prikaži obrazca
   $c = new captcha();
   $k = new kontakt();
  
   if(isset($_POST["send"])) {
    $k->set($_POST["contact"]);
    
    if($_POST["value"] !== "") { //če znesek ni podan, ga ugani
     $znesek = $_POST["value"];
    } else { 
     $znesek = $this->cena($_REQUEST["id"])+1;
    }
    
    if($c->verify() && $this->ponudba($_REQUEST["id"], $znesek, $_POST["contact"])) {
     echo $_POST["contact"].": ".$znesek; ?>&nbsp;€ sprejeto! Vrnite se na <a href="?id=<?php echo rawurlencode($_REQUEST['id']); ?>">prejšnjo stran</a>.<?php
    } else {
     ?>Napaka! Vrnite se na <a href="?id=<?php echo rawurlencode($_REQUEST['id']); ?>">prejšnjo stran</a> in ponovite licitacijo.<?php
    }
   } else {
    $cena = $this->cena($id)+1;
    ?>
     <form method="POST">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      Znesek donacije:<br>
      <input type="number" name="value" placeholder="<?php echo $cena; ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?php echo $cena; ?>'">&nbsp;€<br><br>
      Kontakt na katerega ste dosegljivi:<br>
      <input type="text" name="contact" placeholder="email ali telefonska" onfocus="this.placeholder = ''" onblur="this.placeholder = 'email ali telefonska'" value="<?php echo $k->get(); ?>"><br><br>
      <?php echo $c->show(); ?><div class="captcha-padding"></div><br>
      <input type="submit" name="send" value="Oddaj ponudbo" class="btn btn-primary">
     </form>
     <br><br><br>
    <?php
   }
  }
   
 }
 
 function odprtaSe($id) {
  $l = $this->db->q("
   SELECT `zacetek` 
     FROM `licitacija` 
    WHERE `id` = '".$this->db->e($id)."'
    LIMIT 1
  ");
 
  if(!isset($l[0])) {
   return "Licitacija še ni začeta. Vašo ponudbo lahko oddate na desni.";
  } else {
  
   if($this->mozna($id)) {
      
    $cas = ($this->odprtaDni*24*3600) - (time() - strtotime($l[0]["zacetek"]));
    $out = array();
    if($cas > (24*3600)) {
     $out[] = floor($cas / (24*3600))." dni";
     $cas = $cas % (24*3600);
    }

    if($cas > 3600) {
     $out[] = floor($cas / 3600)." ur";
     $cas = $cas % 3600;
    }
    
    if($cas > 60) {
     $out[] = floor($cas / 60)." minut";
     $cas = $cas % 60;
    }

    /*
    if($cas > 0) {
     $out[] = floor($cas)." sekund";
     $cas = 0;
    }
    */

    return "Licitacija je odprta še ".implode(" ", $out).". Vašo ponudbo lahko oddate na desni.";
   } else {
    return "Licitacija za ta predmet je trenutno že zaključena."; 
   }
   
  }
 
 }
 
 function mozna($id) {
  $l = $this->db->q("
   SELECT `zacetek` 
     FROM `licitacija` 
    WHERE `id` = '".$this->db->e($id)."'
    LIMIT 1
  ");
  
  if(!isset($l[0])) {
   return True;
  } else {
   return (strtotime($l[0]["zacetek"]) > strtotime("-".$this->odprtaDni." days"));
  }
  
 }

}

?>
