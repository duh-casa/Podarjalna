<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Podarjalna.

    Podarjalna is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Podarjalna is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Podarjalna.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

class dblink {

 public $link;

 function __construct($database = "", $user = "", $pass = "", $host = "") {

  include "dbLogin.php";
  foreach($credentials as $key => $value) {
   if(!isset(${$key}) || ${$key} == "") {
    ${$key} = $value;
   }
  }

  $this->link = new mysqli($host, $user, $pass, $database);
  $this->link->set_charset("utf8");

 }
 
 function q($q, $primary = "") {
    
  $r = $this->link->query($q);
  echo $this->link->error;

  $results = array();
  if($r !== True && $r !== False) {
   while($tmp = $r->fetch_assoc()) {
    if($primary == "") {
     $results[] = $tmp;
    } else {
     $results[$tmp[$primary]] = $tmp;
    }
   }
   return $results;
  } else {
   return $this->link->affected_rows;
  }

 }
 
 function e($e) {
  return $this->link->real_escape_string($e);
 }

 function ae($polje) {
  $tmp = array();
  foreach($polje as $kljuc => $vrednost) {
   $tmp[$kljuc] = $this->e($vrednost);
  }
  return $tmp;
 }

 function flatten($array = array(), $parameter = "") {
  if($parameter == "") {
   reset($array);
   $tmp = $array[key($array)];
   reset($tmp);
   $parameter = key($tmp);
  }

  $out = array();
  foreach($array as $vnos) {
   $out[] = $vnos[$parameter]; 
  }

  return $out;
 }

 function __destruct() {
  $this->link->close();
 }

}

?>
