<?php

/*

    Copyright 2016 Jure Sah <dustwolfy@gmail.com>

    This file is part of Podarjalna.

    Podarjalna is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Podarjalna is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Podarjalna.  If not, see <http://www.gnu.org/licenses/>.
    
    Please refer to the README file for additional information.

*/

require_once "mysqli.php";

class items {

	private $db;

	function __construct() {
		$this->db = new dblink();
	}
	 
	function getItems($linki = True) {
	?>
	 	<div class="items">
	 		<?php 
	 			foreach($this->db->q("SELECT `id` FROM `izdelki` ORDER BY RAND()") as $o) {
	 				$this->getItem($o["id"], $linki, False);
	 			}
	 		?>
	 	</div>
	<?php
	}
	 
	function getItem($id, $linki = True, $large = True) {	
	?>
	 <?php if(!$large) { 
	 
	 	$d = $this->db->q("
	   SELECT `ime`, `slika` 
	     FROM `izdelki` 
	    WHERE `id` = '".$this->db->e($id)."' 
	    LIMIT 1
	 ");

	 ?>
 		<?php if($linki) { ?><a href="?id=<?php echo rawurlencode($id); ?>"><?php } ?>
		 	<div class="item">
		 		<div class="item-image" style="background-image: url('slike/<?php echo $d[0]['slika']; ?>');"></div>
		 		<div class="item-name"><?php echo $d[0]['ime']; ?></div>
		 	</div>
	  <?php if($linki) { ?></a><?php } ?>
	 <?php } else { 
	 
	  $d = $this->db->q("
	   SELECT `ime`, `slika`, `opis`, `ohranjenost`
	     FROM `izdelki` 
	    WHERE `id` = '".$this->db->e($id)."' 
	    LIMIT 1
	  ");

   $l = new licitacija();
   
   $cena = $l->cena($id);
   if($cena == 0) {
    $cena = 1;
   }

	 ?>
	 	<div class="item-large">
	 		<div class="item-name-large"><h2><?php echo $d[0]['ime']; ?></h2></div>
	 		<div class="item-image-large" style="background-image: url('slike/<?php echo $d[0]['slika']; ?>');"></div>
	 		<div class="item-details-large">
	 		 <h3>Opis</h3><p><?php echo $d[0]['opis']; ?></p>
	 		 <h3>Ohranjenost</h3><p><?php echo $d[0]['ohranjenost']; ?></p>
	 		 <h3>Cena</h3><p>Trenutna izklicna cena je <span class="item-price-large"><?php echo $cena; ?>&nbsp;€.</span><br><?php echo $l->odprtaSe($id); ?></p>
	 		</div>
	 		<div class="item-buttons-large">
     <?php       
      $l->obrazec($id);           
     ?>
	 		</div>
	 	</div>		 
	 <?php } ?>
	<?php	 
	}
		
}

?>
